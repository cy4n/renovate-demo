FROM openjdk:11.0.9-jre
EXPOSE 8080

RUN mkdir /app
COPY target/broken*.jar /app/broken.jar
WORKDIR /app
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/broken.jar"]
